/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.cache.startup;

import com.neueda.cache.utils.CacheHelper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

/**
 *
 * @author nefeli
 */
public abstract class AbstractStartUpIgnition {
    
    private static final Logger logger = Logger.getLogger(AbstractStartUpIgnition.class.getName());
    
    protected abstract String getConfigLocation();
    protected abstract boolean loadCacheOnStartup();
    protected static final String GRID_NAME = CacheHelper.getGridName();
    
    @PostConstruct
    public void initialize() {
        logger.info("Initiliazing ignite cache!!!");

        Ignite ignite = Ignition.start(getConfigLocation());

        if(loadCacheOnStartup()){
            loadAllCaches(ignite);
        }

        logger.info("Ignite cache initialized!!!");

    }
    
    @PreDestroy
    public void ignitionDestroyed() {
        Ignition.stop(GRID_NAME, true);
    }
    
    protected void loadAllCaches(Ignite ignite){
        ignite.cacheNames().parallelStream().forEach(name -> {
            logger.log(Level.INFO, "Loading cache : {0}", name);

            ignite.cache(name).loadCache(null, "select * from "+CacheHelper.getCassandraTable(name));
        });
    }
}
