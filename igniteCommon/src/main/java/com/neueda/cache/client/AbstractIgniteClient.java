/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.cache.client;

import com.neueda.cache.IgniteCacheNames;
import com.neueda.cache.utils.CacheHelper;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;

/**
 *
 * @author nefeli
 * @param <K>
 * @param <T>
 */
public abstract class AbstractIgniteClient<K, T> {

    protected abstract Class getCachedObjectClass();

    public String getGridName() {
        return CacheHelper.getGridName();
    }

    /**
     * Invokes method getCache(String cacheName)
     *
     * @return IgniteCache
     */
    public IgniteCache getCache() {
        return getCache(IgniteCacheNames.getCacheNameByClass(getCachedObjectClass()));
    }

    /**
     * Returns an existing or creates an IgniteCache with the given name
     *
     * @param cacheName
     * @return IgniteCache
     */
    protected IgniteCache getCache(String cacheName) {
        return Ignition.ignite(getGridName()).cache(cacheName);
    }

    /**
     * Returns the value from the cache based on the given key
     *
     * @param key : The key for each entry to the cache
     * @return value : returns the value from the entry set for the specific key
     */
    public T get(K key) {
        return (T) getCache().get(key);
    }

    /**
     * Adds a new Object to our cache
     *
     * @param key : The unique identifier of the value
     * @param value : Corresponds to one of our entities
     */
    public void put(K key, T value) {

        getCache().put(key, value);

    }

    /**
     * Replace the value from the cache with the updated one based on the given
     * key
     *
     * @param key :The unique identifier of the value
     * @param value : Corresponds to one of our entities
     * @return value : the updated value
     */
    public T edit(K key, T value) {

        return (T) getCache().getAndReplace(key, value);
    }

    /**
     * Removes an object from our cache
     *
     * @param key : Corresponds to key value from entry set
     * @return boolean : true or false
     */
    public boolean delete(K key) {
        //Throws Illegal argument Exception for UrlContract object without null check
        if (get(key) == null) {
            return false;
        }
        return getCache().remove(key);
    }

    public void removeAll() {
        if (getCache() != null) {
            getCache().removeAll();
        }
    }


}
