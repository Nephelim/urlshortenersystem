/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.cache.utils;

import org.apache.ignite.cache.store.cassandra.datasource.Credentials;

/**
 *
 * @author nefeli
 */
public class CassandraAdminCredentials implements Credentials {

    @Override public String getUser() {
        return CassandraHelper.getAdminUser();
    }

    @Override public String getPassword() {
        return CassandraHelper.getAdminPassword();
    }
}
