/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.cache.utils;

import java.util.ResourceBundle;

/**
 *
 * @author nefeli
 */
public class CacheHelper {
    private static final ResourceBundle CONNECTION = ResourceBundle.getBundle("com/neueda/cassandra/connection");
    private static final ResourceBundle CACHE = ResourceBundle.getBundle(CONNECTION.getString("cache.config.path"));

    private CacheHelper() {
    }
    
    public static String getGridName() {
        return CACHE.getString("gridName");
    }
    
    public static String getCassandraTable(String cacheName){
        return CACHE.getString(cacheName);
    }
}
