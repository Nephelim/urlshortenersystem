/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.cache;

/**
 *
 * @author nefeli
 */
public final class IgniteCacheNames {
    
    private static final String SUFFIX = "cache";

    private IgniteCacheNames() {
    }
    
    public static String getCacheNameByClass(Class objectClass){
        return objectClass.getSimpleName()+SUFFIX;
    }
}
