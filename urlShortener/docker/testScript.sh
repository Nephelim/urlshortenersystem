set -e

pwd="$(dirname "$0")"

cd $pwd

mvn_status() 
{
	status=$?
	if [ $status -ne 0 ]; then
        echo "Failed to buld $1: $status"
        exit $status
        fi
}

cp_status()
{
	status=$?
	if [ $status -ne 0 ]; then
  	echo "Failed to cp $1: $status"
  	exit $status
	fi

}


echo "#### Building urlShortener System ####"

mvn -f ../../igniteCommon clean install 
mvn_status igniteCommon


mvn -f ../../urlShortener clean install 
mvn_status urlShortener


# cp
cp ../../urlShortener/target/urlShortener-swarm.jar .
cp_status urlShortener


docker build -t urlshortenersystem .



cd ..

parentdir="$(pwd)"

if [ ! -e "$parentdir/logs" ]
then
        mkdir "$parentdir/logs"
	echo "Logs folder created at $parentdir/logs"
fi



echo "### Execute the commands below to run the cassandra and urlShortener image ###"

echo "docker run --name cassandra -p 9042:9042 -d cassandra:latest"
echo "docker container run --name urlShortener -it -v  $parentdir/logs:/logs -p 8080:8080 urlshortenersystem"
