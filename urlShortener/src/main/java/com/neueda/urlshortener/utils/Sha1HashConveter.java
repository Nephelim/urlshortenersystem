/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.urlshortener.utils;

import java.util.function.Function;
import org.apache.commons.codec.digest.DigestUtils;




/**
 *
 * @author nefeli
 */

public class Sha1HashConveter {
    
    public static Function<String, String> sha1Convertion=(longUrl)->  DigestUtils.sha1Hex(longUrl);
    
    public static Function<String, String> shortSha1Convertion=(sha1)->  sha1.substring(0, 10);
    
    
    public static String constructHashKey(String longUrl){     
      return shortSha1Convertion.apply( sha1Convertion.apply(longUrl));
    }
}
