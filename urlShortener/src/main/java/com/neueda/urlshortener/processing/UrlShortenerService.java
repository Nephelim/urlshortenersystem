/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.urlshortener.processing;

import com.neueda.entities.UrlContract;
import com.neueda.rest.entities.ShortenerRequest;
import com.neueda.rest.entities.ShortenerResponse;
import com.neueda.rest.hateoas.HateoasUriCreation;
import com.neueda.urlshortener.utils.Sha1HashConveter;
import com.neueda.urlshortener.utils.UrlValidator;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author nefeli
 */
@Stateless
public class UrlShortenerService {

    @EJB
    private UrlContractPersist urlContractPersist;

    private static final Logger logger = Logger.getLogger(UrlShortenerService.class.getName());

    public ShortenerResponse processShortenerRequest(UriInfo uriInfo, ShortenerRequest request) {
        if (UrlValidator.isValidUrl(request.getLongUrl())) {
            String hash = Sha1HashConveter.constructHashKey(request.getLongUrl());
            UrlContract contract = urlContractPersist.getOrInsertUrlContarct(request, hash);
            String shortUrl = new HateoasUriCreation().constructUrlWithPathParams(uriInfo, Arrays.asList(hash));
            return new ShortenerResponse(shortUrl, contract);

        }
        return new ShortenerResponse("Invalid url input");
    }

    public String processRedirectionRequest(String hash) {
        return urlContractPersist.getLongUrl(hash);
    }
    
    public List<ShortenerResponse> retrieveAllUrls(UriInfo uriInfo){
       String baseUrl= HateoasUriCreation.getBaseUrl(uriInfo);
       return urlContractPersist.getAllUrlsRecords().parallelStream()
                .map(e -> new ShortenerResponse(HateoasUriCreation.constructShortUrl(baseUrl,
                        Arrays.asList(e.getKey())), e.getValue()))
                .collect(Collectors.toList());
    }

}
