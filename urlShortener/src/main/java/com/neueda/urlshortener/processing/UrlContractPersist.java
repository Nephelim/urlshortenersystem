/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.urlshortener.processing;

import com.neueda.cache.client.ShortUrlClient;
import com.neueda.entities.UrlContract;
import com.neueda.rest.entities.ShortenerRequest;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import javax.cache.Cache;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author nephelim
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class UrlContractPersist {

    private final ShortUrlClient shortUrlClient = new ShortUrlClient();

    private Consumer<UrlContract> setTotalclicks = (urlContract) -> {
        int totalClicks = urlContract.getInvocations();
        urlContract.setLastVisited(new Date());
        urlContract.setInvocations(++totalClicks);
    };

    public String getLongUrl(String hash) {
        UrlContract contract = shortUrlClient.get(hash);
        if (contract != null) {
            updateUrlContractInvocations(hash, contract);
            return contract.getLongUrl();
        }
        return "";
    }

    public UrlContract getOrInsertUrlContarct(ShortenerRequest request, String hash) {
        UrlContract contract = shortUrlClient.get(hash);
        if (contract == null) {
            contract = new UrlContract(request.getLongUrl(), new Date(), request.getDescription(), 0);
            shortUrlClient.put(hash, contract);
        }
        return contract;
    }

    public List<Cache.Entry<String, UrlContract>> getAllUrlsRecords() {
        return shortUrlClient.getAll();
    }

    private void updateUrlContractInvocations(String hash, UrlContract contract) {
        setTotalclicks.accept(contract);
        shortUrlClient.edit(hash, contract);
    }

}
