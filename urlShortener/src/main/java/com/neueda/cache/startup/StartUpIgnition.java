package com.neueda.cache.startup;

import javax.ejb.Singleton;
import javax.ejb.Startup;


/**
 *
 * @author nefeli
 */
@Singleton
@Startup
public class StartUpIgnition extends AbstractStartUpIgnition{

    @Override
    protected String getConfigLocation() {
        return "com/neueda/cache/ignite-config.xml";
    }

    @Override
    protected boolean loadCacheOnStartup() {
        return true;
    }
}