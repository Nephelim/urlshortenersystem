/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.cache.client;

import com.neueda.entities.UrlContract;
import java.util.ArrayList;
import java.util.List;
import javax.cache.Cache;
import javax.cache.Cache.Entry;
import org.apache.ignite.cache.query.SqlQuery;

/**
 *
 * @author nefeli
 */
public class ShortUrlClient extends AbstractIgniteClient<String, UrlContract> {

    @Override
    protected Class getCachedObjectClass() {
        return UrlContract.class;
    }

    public List<Entry<String, UrlContract>> getAll() {
        List<Cache.Entry<String, UrlContract>> urls = getCache().query(new SqlQuery(UrlContract.class, "select * from UrlContract;")).getAll();
        if (urls == null) {
            urls = new ArrayList<>();
        }
        return urls;
    }
}
