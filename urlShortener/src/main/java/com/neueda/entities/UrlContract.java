/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 *
 * @author nefeli
 */
public class UrlContract {

    @NotNull
    @Size(min = 5)
    @QuerySqlField
    private String longUrl;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private Date dateCreated;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private Date lastVisited;

    @NotNull
    private String description;

    private int invocations;

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastVisited() {
        return lastVisited;
    }

    public void setLastVisited(Date lastVisited) {
        this.lastVisited = lastVisited;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getInvocations() {
        return invocations;
    }

    public void setInvocations(int invocations) {
        this.invocations = invocations;
    }

    public UrlContract(String longUrl, String description) {
        this.longUrl = longUrl;
        this.description = description;
        this.dateCreated = new Date();
    }

    public UrlContract(String longUrl, Date dateCreated, String description, int totalClicks) {
        this.longUrl = longUrl;
        this.dateCreated = dateCreated;
        this.description = description;
        this.invocations = totalClicks;
    }

    public UrlContract() {
    }

}
