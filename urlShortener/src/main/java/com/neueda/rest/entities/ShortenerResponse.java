/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.rest.entities;

import com.neueda.entities.UrlContract;

/**
 *
 * @author nephelim
 */
public class ShortenerResponse {

    private String shortUrl;
    private String error;

    private UrlContract urlContract;

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public UrlContract getUrlContract() {
        return urlContract;
    }

    public void setUrlContract(UrlContract urlContract) {
        this.urlContract = urlContract;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ShortenerResponse(String shortUrl, UrlContract urlContract) {
        this.urlContract = urlContract;
        this.shortUrl = shortUrl;
    }

    public ShortenerResponse(String error) {
        this.error = error;
    }
    
    

}
