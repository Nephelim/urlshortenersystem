/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.rest.entities;

import java.io.Serializable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author nefeli
 */
public class ShortenerRequest {

    private String longUrl;
    
    private String description;

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
