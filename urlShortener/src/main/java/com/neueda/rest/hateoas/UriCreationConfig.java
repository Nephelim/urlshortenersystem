/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.rest.hateoas;

import java.util.List;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author nefeli
 */
public interface UriCreationConfig {

    public UriBuilder constructUriWithPathParams(UriBuilder uri, List<String> params);

    public String constructUrlWithPathParams(UriInfo uri, List<String> params);
}
