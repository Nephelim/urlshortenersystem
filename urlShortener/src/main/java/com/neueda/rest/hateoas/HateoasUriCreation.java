/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.rest.hateoas;

import java.util.List;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author nefeli
 */
public class HateoasUriCreation implements UriCreationConfig {

    @Override
    public UriBuilder constructUriWithPathParams(UriBuilder uri, List<String> params) {
        if (params != null) {
            for (String pathParam : params) {
                uri = uri.path(pathParam);
            }
        }
        return uri;
    }

    @Override
    public String constructUrlWithPathParams(UriInfo uriInfo, List<String> params) {
        UriBuilder uri = uriInfo.getBaseUriBuilder();
        return constructUriWithPathParams(uri, params).build().toString();
    }

    public static String getBaseUrl(UriInfo uriInfo) {
       return uriInfo.getBaseUriBuilder().build().toString();
       
    }

    public static String constructShortUrl(String baseUrl, List<String> params) {
        StringBuilder sb = new StringBuilder(baseUrl);
        for(String param : params){
            sb.append(param).append("/");
        }
       return sb.toString();
       
    }
}
