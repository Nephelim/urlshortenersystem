package com.neueda.rest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;

/**
 *
 * @author nefeli
 */
@ApplicationPath("/")
public class RestApplication extends Application {

    public RestApplication() {
    }

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<>(Arrays.asList(
                com.neueda.rest.ShortenerUrlEndpoint.class,
                com.neueda.rest.ShortenerUrlRedirectionEndpoint.class
        ));
    }
}
