/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.rest;

import com.neueda.urlshortener.processing.UrlShortenerService;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author nefeli
 */
@RequestScoped
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ShortenerUrlRedirectionEndpoint {

    private static final Logger logger = Logger.getLogger(ShortenerUrlRedirectionEndpoint.class.getName());

    @EJB
    private UrlShortenerService urlService;

    @GET
    @Path("{hash}")
    public Response redirect(@PathParam("hash") String hash) {
        try {
            logger.log(Level.INFO, "{0} invoked", getClass().getSimpleName());
            return Response.seeOther(new URI(urlService.processRedirectionRequest(hash))).build();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception", ex);
            return Response.serverError().entity(ex.getMessage()).build();
        }

    }

}
