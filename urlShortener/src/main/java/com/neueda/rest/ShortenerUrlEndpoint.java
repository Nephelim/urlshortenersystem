/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.rest;

import com.neueda.rest.entities.ShortenerRequest;
import com.neueda.rest.entities.ShortenerResponse;
import com.neueda.urlshortener.processing.UrlShortenerService;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author nefeli
 */
@RequestScoped
@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ShortenerUrlEndpoint {

    @EJB
    private UrlShortenerService shortUrlConverterService;

    private static final Logger logger = Logger.getLogger(ShortenerUrlEndpoint.class.getName());

    private Predicate<ShortenerRequest> isInValidRequest = (request) -> (request == null || request.getLongUrl() == null);
    private Predicate<ShortenerResponse> hasErrors = (response) -> (response != null && response.getError()!= null);

    @POST
    @Path("/generator")
    public Response postShortUrls(@Context UriInfo uriInfo, ShortenerRequest request) {
        try {
            logger.log(Level.INFO, "postShortUrls invoked");
            return determineResponse(uriInfo, request);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception", ex);
            return Response.serverError().entity(ex.getMessage()).build();
        }

    }

    @GET
    @Path("/statistics")
    public Response getUrlStatistics(@Context UriInfo uriInfo) {
        try {
            logger.log(Level.INFO, "getUrlStatistics invoked");
            return Response.ok().entity(shortUrlConverterService.retrieveAllUrls(uriInfo)).build();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception", ex);
            return Response.serverError().entity(ex.getMessage()).build();
        }
    }
    

    private Response determineResponse(@Context UriInfo uriInfo, ShortenerRequest request) {
        if (!isInValidRequest.test(request)) {
            ShortenerResponse response = shortUrlConverterService.processShortenerRequest(uriInfo, request);
            if (!hasErrors.test(response) ) {
                return Response.ok(shortUrlConverterService.processShortenerRequest(uriInfo, request)).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity("Oops, invalid input data").build();
    }

}
