/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.urlshortening.processing;

import com.neueda.entities.UrlContract;
import com.neueda.rest.entities.ShortenerRequest;
import com.neueda.rest.entities.ShortenerResponse;
import com.neueda.rest.hateoas.HateoasUriCreation;
import com.neueda.urlshortener.processing.UrlContractPersist;
import com.neueda.urlshortener.processing.UrlShortenerService;
import java.util.Date;
import java.util.List;
import javax.ws.rs.core.UriInfo;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

/**
 *
 * @author nefeli
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({UrlShortenerService.class, UrlContractPersist.class, HateoasUriCreation.class})
public class UrlShortenerServiceTest {

    @Mock
    private UrlContractPersist urlContractPersist;

    @Mock
    private UriInfo uriInfo;

    @Mock
    private HateoasUriCreation hateoasUriCreation;

    private UrlShortenerService urlShortener ;

    public UrlShortenerServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        urlShortener=PowerMockito.spy( new UrlShortenerService());
        PowerMockito.whenNew(HateoasUriCreation.class).withNoArguments().thenReturn(hateoasUriCreation);
        Whitebox.setInternalState(urlShortener, "urlContractPersist", urlContractPersist);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void processShortenerRequest_ValidUrlTest() {
        ShortenerRequest request = new ShortenerRequest();
        request.setLongUrl("https://mail.google.com/");
        request.setDescription("test mail");
        UrlContract contract = new UrlContract("https://mail.google.com/", new Date(), "mail url", 0);
        Mockito.when(urlContractPersist.getOrInsertUrlContarct(Mockito.any(ShortenerRequest.class), Mockito.anyString())).thenReturn(contract);
        Mockito.when(hateoasUriCreation.constructUrlWithPathParams(Mockito.any(UriInfo.class), Mockito.any(List.class))).thenReturn("mail/test");
        ShortenerResponse response= urlShortener.processShortenerRequest(uriInfo, request);
        assertTrue(response.getShortUrl().equals("mail/test"));

    }
    
    @Test
    public void processShortenerRequest_InValidUrlTest() {
        ShortenerRequest request = new ShortenerRequest();
        request.setLongUrl("test");
        request.setDescription("test mail");
        ShortenerResponse response= urlShortener.processShortenerRequest(uriInfo, request);
        assertTrue(response.getError().equals("Invalid url input"));
        assertNull(response.getUrlContract());

    }
}
