/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.urlshortening.processing;

import com.neueda.cache.client.ShortUrlClient;
import com.neueda.entities.UrlContract;
import com.neueda.rest.entities.ShortenerRequest;
import com.neueda.rest.entities.ShortenerResponse;
import com.neueda.urlshortener.processing.UrlContractPersist;
import java.net.URI;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.cache.Cache;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import org.apache.ignite.internal.processors.cache.CacheEntryImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import static org.mockito.Matchers.isA;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author nefeli
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({UrlContractPersist.class, ShortUrlClient.class})
public class UrlContractPersistTest {

    @Mock
    private UrlContractPersist urlContractPersist;

    @Mock
    private ShortUrlClient shortUrlClient;

    public UrlContractPersistTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Logger.class);
        PowerMockito.whenNew(ShortUrlClient.class).withNoArguments().thenReturn(shortUrlClient);
        urlContractPersist = PowerMockito.spy(new UrlContractPersist());
    }

    @After
    public void tearDown() {
    }

    @Test
    public void keyDoesnotExistTest() {
        assertEquals(urlContractPersist.getLongUrl(""), "");

    }

    @Test
    public void keyExistTest() {
        String dummyUrl = "https://mvnrepository.com/";
        UrlContract urlContract = new UrlContract(dummyUrl, new Date(), "test Description", 0);
        Mockito.when(shortUrlClient.get(Matchers.anyObject())).thenReturn(new UrlContract(dummyUrl, new Date(), "test Description", 0));
        assertEquals(urlContractPersist.getLongUrl(""), dummyUrl);
    }

    @Test
    public void getAllUrlContractsTest() {
        UrlContract urlContract = new UrlContract("https://mvnrepository.com/", new Date(), "test Description", 0);
        UrlContract urlContract1 = new UrlContract("https://stackoverflow.com", new Date(), "test Description", 1);
        List dummyresults = Arrays.asList(new CacheEntryImpl<>("testHash1", urlContract),
                new CacheEntryImpl<>("testHash1", urlContract1));

        Mockito.when(shortUrlClient.getAll()).thenReturn(dummyresults);
        UriInfo info = Mockito.mock(UriInfo.class);
        UriBuilder uriBuilder = Mockito.mock(UriBuilder.class);
        URI uri = URI.create("http://local/rest/test");
        Mockito.when(info.getBaseUriBuilder()).thenReturn(uriBuilder);
        Mockito.when((uriBuilder).path(isA(String.class))).thenReturn(uriBuilder);
        Mockito.when((uriBuilder).build()).thenReturn(uri);
        List<Cache.Entry<String, UrlContract>> results = urlContractPersist.getAllUrlsRecords();
        assertTrue(results.size() == 2);
    }

    @Test
    public void getOrInsertUrlContarct_hashExistsTest() {
        String dummyUrl = "https://mvnrepository.com/";
        Mockito.when(shortUrlClient.get(Matchers.anyObject()))
                .thenReturn(new UrlContract(dummyUrl, new Date(), "test Description", 10));
        UrlContract contract = urlContractPersist.getOrInsertUrlContarct(new ShortenerRequest(), "https://mvnrepository.com/");
        assertTrue(contract.getInvocations() == 10);
    }

    @Test
    public void getOrInsertUrlContarct_hashDoesNotExistsTest() {
        ShortenerRequest request = new ShortenerRequest();
        request.setDescription("test");
        request.setLongUrl("https://mvnrepository.com/");
        Mockito.when(shortUrlClient.get(Matchers.anyObject()))
                .thenReturn(null);
        UrlContract contract = urlContractPersist.getOrInsertUrlContarct(request,"12345678");
        assertTrue(contract.getInvocations() == 0);
    }
}
