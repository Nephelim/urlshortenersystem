package com.neueda.rest.hateoas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.URI;
import java.util.Arrays;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import javax.ws.rs.core.UriBuilder;
import static org.junit.Assert.*;
import static org.mockito.Matchers.isA;

/**
 *
 * @author nefeli
 */
public class HateoasUriCreationTest {

    public HateoasUriCreationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.

    @Test
    public void constructUrlWithPathParamsTest() {
        HateoasUriCreation hateoas = new HateoasUriCreation();
        UriInfo info = Mockito.mock(UriInfo.class);
        UriBuilder uriBuilder = Mockito.mock(UriBuilder.class);
        URI uri = URI.create("http://local/rest/test");
        Mockito.when(info.getBaseUriBuilder()).thenReturn(uriBuilder);
        Mockito.when((uriBuilder).path(isA(String.class))).thenReturn(uriBuilder);
        Mockito.when((uriBuilder).build()).thenReturn(uri);
        String url = hateoas.constructUrlWithPathParams(info, Arrays.asList("test"));
        assertTrue(url.equals("http://local/rest/test"));
        Mockito.verify(uriBuilder).path(isA(String.class));
    }

    @Test
    public void constructUrlWithNoPathParamsTest() {
        HateoasUriCreation hateoas = new HateoasUriCreation();
        UriInfo info = Mockito.mock(UriInfo.class);
        UriBuilder uriBuilder = Mockito.mock(UriBuilder.class);
        URI uri = URI.create("http://local/rest/");
        Mockito.when(info.getBaseUriBuilder()).thenReturn(uriBuilder);
        Mockito.when((uriBuilder).path(isA(String.class))).thenReturn(uriBuilder);
        Mockito.when((uriBuilder).build()).thenReturn(uri);
        String url = hateoas.constructUrlWithPathParams(info, null);
        assertTrue(url.equals("http://local/rest/"));
        Mockito.verify(uriBuilder,Mockito.never()).path(isA(String.class));
    }
}
