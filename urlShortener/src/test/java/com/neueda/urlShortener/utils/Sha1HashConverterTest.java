/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neueda.urlShortener.utils;

import com.neueda.urlshortener.utils.Sha1HashConveter;
import com.neueda.urlshortener.utils.UrlValidator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nefeli
 */
public class Sha1HashConverterTest {

    public Sha1HashConverterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void sha1ConvertionTest() {
        String sha1 = Sha1HashConveter.sha1Convertion.apply("https://www.google.gr");
        assertNotEquals("https://www.google.gr", Sha1HashConveter.constructHashKey("https://www.google.gr"));
        assertTrue(sha1.length()==40);
    }
    
    @Test
    public void shortSha1ConvertionTest() {
        String sha1 = Sha1HashConveter.shortSha1Convertion.apply("https://www.google.gr");
        assertNotEquals("https://www.google.gr", Sha1HashConveter.constructHashKey("https://www.google.gr"));
        assertTrue(sha1.length()==10);
    }
}
