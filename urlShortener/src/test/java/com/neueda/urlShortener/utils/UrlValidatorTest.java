package com.neueda.urlShortener.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.neueda.urlshortener.utils.UrlValidator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nefeli
 */
public class UrlValidatorTest {

    public UrlValidatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validUrlTest() {
        assertTrue(UrlValidator.isValidUrl("https://www.google.gr"));
    }

    @Test
    public void invalidUrlTest() {
        assertFalse(UrlValidator.isValidUrl("ht://www.gr"));

    }
}
